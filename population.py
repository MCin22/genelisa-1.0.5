import copy
import random
import PIL
from PIL.ImageDraw import ImageDraw
import numpy as np
import scipy
from fitness_calculator import FitnessCalculator
import settings
import cv2


class Population:
    def __init__(self):
        population_size = settings.SettingsModule.settingsDict['population_size']
        self.individuals = []

        for i in range(0, population_size):
            self.individuals.append(Individual())

    def get_individual(self, index):
        return self.individuals[index]

    def set_individual(self, individual, index):
        self.individuals[index] = individual

    def add_individual(self, individual):
        self.individuals.append(individual)

    def get_best_individual(self):
        best_individual = None
        best_fitness = 100000000000
        for individual in self.individuals:
            if individual.fitness < best_fitness:
                best_individual = individual
                best_fitness = individual.fitness
        return best_individual, best_fitness

    def sort_by_fitness(self):
        sorted_individuals = copy.deepcopy(self.individuals)
        for i in range(1, len(self.individuals)):
            j = i
            while j > 0 and sorted_individuals[j].fitness < sorted_individuals[j - 1].fitness:
                sorted_individuals[j], sorted_individuals[j - 1] = sorted_individuals[j - 1], sorted_individuals[j]
                j -= 1
        return sorted_individuals


class Genome:
    def __init__(self):
        self.genome_code = []
        self.generate_random()

    def generate_random(self):
        genome_length = settings.SettingsModule.settingsDict['genome_length']

        for i in range(0, genome_length):
            self.genome_code.append(FigureFactory.return_triangle())


class Individual(Genome):
    def __init__(self):
        Genome.__init__(self)
        self.fitness = 1000000
        self.image = None

    def calculate_fitness(self):
        self.build_image()
        self.fitness = FitnessCalculator.return_fitness(self)

    def build_image(self):
        # budowanie obrazu na podstawie genomu
        max_x = settings.SettingsModule.settingsDict['image_x']
        max_y = settings.SettingsModule.settingsDict['image_y']

        # wersja dla nieprzezroczystych trojkatow - najszybsza (nalezy w settings ustawic parametry min i max
        # alpha na 255 - wymaga OpenCV
        # im = np.zeros((max_y, max_x, 4), np.uint8)
        # im[:, :, 3] = 255
        # for figure in self.genome_code:
        #     fill_color = (figure.color['R'], figure.color['G'], figure.color['B'], figure.color['A'])
        #     points = figure.return_tuple_points()
        #     pts = np.asarray(points, np.int32)
        #     # cv2.fillPoly(im, [pts], fill_color)
        #     cv2.fillConvexPoly(im, pts, fill_color)
        # self.image = im

        # wersja wolniejsza, z troche inna obsluga przezroczystosci - wymaga OpenCV
        # im_final = np.zeros((max_y, max_x, 4), np.uint8)
        # im_final[:, :, 3] = 255
        # for figure in self.genome_code:
        #     im = np.zeros((max_y, max_x, 4), np.uint8)
        #     fill_color = (figure.color['R'], figure.color['G'], figure.color['B'], figure.color['A'])
        #     points = figure.return_tuple_points()
        #     pts = np.asarray(points, np.int32)
        #     # cv2.fillPoly(im, [pts], fill_color)
        #     cv2.fillConvexPoly(im, pts, fill_color)
        #     im_final = cv2.addWeighted(im_final, 1, im, 0.5, 0)
        #     # im_final = cv2.add(im_final, im)
        # self.image = im_final

        # wersja wolniejsza, z prawidlowa obsluga przezroczystosci - wymaga Pillow
        im = PIL.Image.new("RGBA", (max_x, max_y))
        im_final = PIL.Image.new("RGBA", (max_x, max_y), (0, 0, 0, 255))
        im_draw = PIL.ImageDraw.Draw(im, 'RGBA')
        for figure in self.genome_code:
            fill_color = (figure.color['R'], figure.color['G'], figure.color['B'], figure.color['A'])
            points = figure.return_tuple_points()
            im_draw.polygon(points, fill_color)
            im_final = PIL.Image.alpha_composite(im_final, im)
            im = PIL.Image.new("RGBA", (max_x, max_y))
            im_draw = PIL.ImageDraw.Draw(im, 'RGBA')
        self.image = np.asarray(im_final)


class FigureFactory:
    def __init__(self):
        pass

    @staticmethod
    def return_triangle():
        return Triangle()


class Figure:
    def __init__(self):
        mutation_color_alpha_range_min = settings.SettingsModule.settingsDict['mutation_color_alpha_range_min']
        mutation_color_alpha_range_max = settings.SettingsModule.settingsDict['mutation_color_alpha_range_max']

        self.color = {'A': random.randint(mutation_color_alpha_range_min, mutation_color_alpha_range_max),
                      'R': random.randint(0, 255), 'G': random.randint(0, 255),
                      'B': random.randint(0, 255)}

    def return_tuple_points(self):
        pass


class Triangle(Figure):
    def __init__(self):
        Figure.__init__(self)

        genome_length = np.sqrt(settings.SettingsModule.settingsDict['genome_length'])
        self.points = []
        max_x = settings.SettingsModule.settingsDict['image_x']
        max_y = settings.SettingsModule.settingsDict['image_y']
        step_size = np.sqrt(np.sqrt((max_x * max_y) / genome_length))

        point_1 = Point()
        point_2 = Point()
        point_3 = Point()

        point_2.x = min(max(0, point_1.x + self.get_random(0, step_size)), max_x)
        point_2.y = min(max(0, point_1.y + self.get_random(-step_size, step_size)), max_y)

        point_3.x = min(max(0, point_1.x + self.get_random(-step_size, step_size)), max_x)
        point_3.y = min(max(0, point_1.y + self.get_random(0, step_size)), max_y)

        self.points.append(point_1)
        self.points.append(point_2)
        self.points.append(point_3)

    def get_random(self, min, max):
        return random.randint(int(min), int(max))

    def return_tuple_points(self):
        tuple_points = [(self.points[0].x, self.points[0].y), (self.points[1].x, self.points[1].y),
                        (self.points[2].x, self.points[2].y)]
        return tuple_points


class Point:
    def __init__(self):
        max_x = settings.SettingsModule.settingsDict['image_x']
        max_y = settings.SettingsModule.settingsDict['image_y']
        self.x = random.randint(0, max_x)
        self.y = random.randint(0, max_y)
