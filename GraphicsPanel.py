from PySide import QtGui, QtCore
from PySide.QtGui import QPainterPath
import numpy as np
__author__ = 'przezdomskia'



class PanelHolder(QtGui.QGraphicsView):
    ''' '''
    def __init__(self):
        super(PanelHolder, self).__init__()
        self.setTransformationAnchor(QtGui.QGraphicsView.AnchorUnderMouse)
        self.scene = QtGui.QGraphicsScene()
        self.setScene(self.scene)
        self.lastClickedImageNumber = -1
        self.currentIndex = -1
        self.panelList = []
        self.contentPanel = None
        self.lastDragPoint = (0,0)
        self.lastMousePoint = None
        self.zoomSpeed = 1.25 # 125%
        self.currentZoom = 1.0
        self.setFrameShape(QtGui.QFrame.WinPanel)
        self.setFrameShadow(QtGui.QFrame.Sunken)
        self.controlKeyPressed = False
        self.currentTransform = None
        self.scene.mousePressEvent = self.mousePressEventC
        self.scene.mouseMoveEvent = self.mouseMoveEventC
        self.add_panel(ImagePanel())
        self.set_active_panel_index(0)

    def scrollContentsBy(self, dx, dy):
        super(PanelHolder, self).scrollContentsBy(dx, dy)
        self.scene.update()

    def add_panel(self, panel):
        '''add panel to scroll holder'''
        self.panelList.append(panel)
        pass

    def set_active_panel_index(self, index):
        '''change actually displayed panel to panel with specified index number'''
        size = len(self.panelList)
        if size<0 or index >= size:
            raise Exception('panel index out of bounds exception ',str(index))
        self.currentIndex = index
        if self.contentPanel is not None:
            self.scene.removeItem(self.contentPanel)
        self.contentPanel = self.panelList[index]
        self.scene.addItem(self.contentPanel)
        contentRect = self.contentPanel.boundingRect()
        self.scene.setSceneRect(contentRect)
        self.scene.update()
        pass

    def mousePressEventC(self, event):
        if self.contentPanel is None:
            return;

    def wheelEvent(self, event):
        if self.contentPanel is None:
            return
        t = self.viewportTransform()
        t = t.inverted()[0]
        point = t.map(event.pos())
        if event.delta() < 0:
            self.currentZoom *= 2-self.zoomSpeed
        else:
            self.currentZoom *= self.zoomSpeed
        t = QtGui.QTransform.fromScale(self.currentZoom, self.currentZoom)
        self.setTransform(t)
        self.update()
        pass

    def mouseMoveEventC(self, event):
        if event.buttons() == QtCore.Qt.RightButton:
            point = event.scenePos()
            moveX = self.lastDragPoint[0] - point.x()
            moveY = self.lastDragPoint[1] - point.y()
            self.lastDragPoint = (point.x()+moveX, point.y()+moveY)
            self.verticalScrollBar().setValue(self.verticalScrollBar().value()+moveY)
            self.horizontalScrollBar().setValue(self.horizontalScrollBar().value()+moveX)

    def on_zoomValue_update_ui(self, newValueForUI, propertyController):
        ''' set panel zoom to specified
        :param zoomValue: number in floating point values form 0, 1 is default size
        '''
        self.currentZoom = newValueForUI
        if self.contentPanel is None:
            return
        image_result = self.contentPanel.displayResult
        if image_result is not None:
            t = QtGui.QTransform.fromScale(self.currentZoom,self.currentZoom)
            self.setTransform(t)
            self.update()



class ImagePanel(QtGui.QGraphicsItem):
    '''
    class for diaplaying generated images
    '''

    def __init__(self):
        super(ImagePanel, self).__init__()
        self.xy = QtCore.QPoint(0, 0)
        self.image_size_x = 100
        self.image_size_y = 100
        ix = (self.image_size_x+3)//4*4
        iy = (self.image_size_y+3)//4*4
        self.image_2d = np.zeros((ix*iy,), np.uint16)  # only in this form it working correctly, do not use array 2D
        pass

    def paint(self, painter, option, widget):
        '''
        method for drawing panel content
        :param context: Grapgics context, use it for drawing
        '''
         # #draw images

       # painter.drawPixmap(0,0, self.image_2d)
        painter.setPen(QtGui.QColor(250, 0, 0))
        painter.setRenderHint(QtGui.QPainter.Antialiasing)
        painter.drawEllipse(QtCore.QPoint(0, 0), 100, 100)

        currentColor = QtGui.QBrush(QtGui.QColor("blue"))
        self.fillTriangle(painter, (0,0,100,190,0,150), currentColor)
        pass

    def boundingRect(self):
        return QtCore.QRectF(0, 0, self.image_size_x, self.image_size_y)

    def fillTriangle(self, painter, bounds, color):
        path = QtGui.QPainterPath()
        path.moveTo(bounds[0], bounds[1])
        path.lineTo(bounds[2], bounds[3])
        path.lineTo(bounds[4], bounds[5])
        path.lineTo(bounds[0], bounds[1])
        painter.fillPath(path, color)