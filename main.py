import PIL
import sys
from scipy import misc
import scipy
from algorithm import Algorithm
from fitness_calculator import FitnessCalculator
from population import Population
from settings import SettingsModule


# http://www.theprojectspot.com/tutorial-post/creating-a-genetic-algorithm-for-beginners/3
# poradnik, jak w Javie napisac podstawowy algorytm genetyczny

def do_work():
    # for NumPy version
    SettingsModule.loaded_image = scipy.misc.imread('mona.png')
    SettingsModule.settingsDict['image_x'] = SettingsModule.loaded_image.shape[1]
    SettingsModule.settingsDict['image_y'] = SettingsModule.loaded_image.shape[0]

    FitnessCalculator.original_image = SettingsModule.loaded_image

    generation_counter = 0
    population = Population()
    best_fitness = 100000000

    while best_fitness > 0:
        new_population = Algorithm.evolve(population)
        best_individual, best_fitness = new_population.get_best_individual()
        print str(generation_counter) + " " + str(best_fitness)
        population = new_population
        generation_counter += 1

        id = SettingsModule.individual_id
        SettingsModule.individual_id += 1
        if generation_counter % 400 == 0:
            misc.imsave('out/out' + str(id) + '.png', best_individual.image)  # for NumPy version


def initialize_modules():
    SettingsModule.initialize_settings()


def main():
    initialize_modules()
    do_work()
    sys.exit()

if __name__ == '__main__':
    main()