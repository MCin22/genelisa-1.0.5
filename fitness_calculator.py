from datetime import datetime
import numpy as np
import settings


class FitnessCalculator:
    def __init__(self):
        pass

    original_image = settings.SettingsModule.loaded_image

    @classmethod
    def return_fitness(cls, individual):
        # porownuje obraz oryginalny z nowym obrazem
        fitness = 0

        temp = np.subtract(cls.original_image, individual.image)
        temp = temp.astype('uint16')
        # temp = np.abs(temp)
        temp = temp * temp
        fitness = np.sum(temp[:, :, 0:2]) / (settings.SettingsModule.settingsDict['image_x'] * settings.SettingsModule.settingsDict['image_y'])

        # fitness /= 1000000.0
        return fitness
