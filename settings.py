class SettingsModule:
    def __init__(self):
        pass

    settingsDict = {}
    loaded_image = None

    individual_id = 0

    @classmethod
    def initialize_settings(cls):
        cls.settingsDict['population_size'] = 1
        cls.settingsDict['genome_length'] = 300
        cls.settingsDict['image_x'] = 100
        cls.settingsDict['image_y'] = 100

        cls.settingsDict['mutation_move_point_max_rate'] = 150
        cls.settingsDict['mutation_move_point_mid_rate'] = 150
        cls.settingsDict['mutation_move_point_min_rate'] = 150

        cls.settingsDict['mutation_move_point_range_mid'] = 20
        cls.settingsDict['mutation_move_point_range_min'] = 3

        cls.settingsDict['mutation_color_red_rate'] = 150
        cls.settingsDict['mutation_color_green_rate'] = 150
        cls.settingsDict['mutation_color_blue_rate'] = 150
        cls.settingsDict['mutation_color_alpha_rate'] = 150

        cls.settingsDict['mutation_color_alpha_range_min'] = 60
        cls.settingsDict['mutation_color_alpha_range_max'] = 120

        # default settings
        # cls.settingsDict['mutation_move_point_max_rate'] = 1500
        # cls.settingsDict['mutation_move_point_mid_rate'] = 1500
        # cls.settingsDict['mutation_move_point_min_rate'] = 1500
        #
        # cls.settingsDict['mutation_move_point_range_mid'] = 20
        # cls.settingsDict['mutation_move_point_range_min'] = 3
        #
        # cls.settingsDict['mutation_color_red_rate'] = 1500
        # cls.settingsDict['mutation_color_green_rate'] = 1500
        # cls.settingsDict['mutation_color_blue_rate'] = 1500
        # cls.settingsDict['mutation_color_alpha_rate'] = 1500
        #
        # cls.settingsDict['mutation_color_alpha_range_min'] = 30
        # cls.settingsDict['mutation_color_alpha_range_max'] = 60
