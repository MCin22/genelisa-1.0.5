import copy
import random
from population import Population, Individual
import settings


class Algorithm:
    def __init__(self):
        pass

    @classmethod
    def evolve(cls, population):
        # tworzona jest nowa populacja i populacja posortowana
        new_population = Population()
        new_population.individuals = []

        new_individual = copy.deepcopy(population.individuals[0])
        new_individual = cls.mutate(new_individual)
        new_individual.calculate_fitness()
        new_population.add_individual(new_individual)

        if new_population.individuals[0].fitness < population.individuals[0].fitness:
            return new_population
        else:
            return population

    @classmethod
    def get_random(cls, min, max):
        return random.randint(min, max)

    @classmethod
    def will_mutate(cls, mutation_rate):
        if cls.get_random(0, mutation_rate) == 1:
            return True
        else:
            return False

    # metoda mutujaca kolor i ksztalt pojedynczej figury losujac nowe wartosci
    @classmethod
    def mutate(cls, individual):
        max_x = settings.SettingsModule.settingsDict['image_x']
        max_y = settings.SettingsModule.settingsDict['image_y']

        # mutation_rate = settings.SettingsModule.settingsDict['mutation_rate']
        mutation_move_point_max_rate = settings.SettingsModule.settingsDict['mutation_move_point_max_rate']
        mutation_move_point_mid_rate = settings.SettingsModule.settingsDict['mutation_move_point_mid_rate']
        mutation_move_point_min_rate = settings.SettingsModule.settingsDict['mutation_move_point_min_rate']

        mutation_move_point_range_mid = settings.SettingsModule.settingsDict['mutation_move_point_range_mid']
        mutation_move_point_range_min = settings.SettingsModule.settingsDict['mutation_move_point_range_min']

        mutation_color_red_rate = settings.SettingsModule.settingsDict['mutation_color_red_rate']
        mutation_color_green_rate = settings.SettingsModule.settingsDict['mutation_color_green_rate']
        mutation_color_blue_rate = settings.SettingsModule.settingsDict['mutation_color_blue_rate']
        mutation_color_alpha_rate = settings.SettingsModule.settingsDict['mutation_color_alpha_rate']

        mutation_color_alpha_range_min = settings.SettingsModule.settingsDict['mutation_color_alpha_range_min']
        mutation_color_alpha_range_max = settings.SettingsModule.settingsDict['mutation_color_alpha_range_max']

        for i in range(0, len(individual.genome_code)):
            for point in individual.genome_code[i].points:
                # if cls.will_mutate(mutation_move_point_max_rate):
                #     point.x = random.randint(0, max_x)
                #     point.y = random.randint(0, max_y)

                # if cls.will_mutate(mutation_move_point_mid_rate):
                #     point.x = min(max(0, point.x + cls.get_random(-mutation_move_point_range_mid, mutation_move_point_range_mid)), max_x)
                #
                # if cls.will_mutate(mutation_move_point_mid_rate):
                #     point.y = min(max(0, point.y + cls.get_random(-mutation_move_point_range_mid, mutation_move_point_range_mid)), max_y)

                if cls.will_mutate(mutation_move_point_min_rate):
                    point.x = min(max(0, point.x + cls.get_random(-mutation_move_point_range_min, mutation_move_point_range_min)), max_x)

                if cls.will_mutate(mutation_move_point_min_rate):
                    point.y = min(max(0, point.y + cls.get_random(-mutation_move_point_range_min, mutation_move_point_range_min)), max_y)

            if cls.will_mutate(mutation_color_red_rate):
                individual.genome_code[i].color['R'] = random.randint(0, 255)

            if cls.will_mutate(mutation_color_green_rate):
                individual.genome_code[i].color['G'] = random.randint(0, 255)

            if cls.will_mutate(mutation_color_blue_rate):
                individual.genome_code[i].color['B'] = random.randint(0, 255)

            if cls.will_mutate(mutation_color_alpha_rate):
                individual.genome_code[i].color['A'] = random.randint(mutation_color_alpha_range_min, mutation_color_alpha_range_max)

        return individual
